package com.prime.todo_project.domain.task.service;

import com.prime.todo_project.domain.project.entity.Project;
import com.prime.todo_project.domain.project.repository.ProjectRepository;
import com.prime.todo_project.domain.task.dto.TaskRequestDTO;
import com.prime.todo_project.domain.task.dto.TaskResponseDTO;
import com.prime.todo_project.domain.task.entity.Task;
import com.prime.todo_project.domain.task.mapper.TaskMapper;
import com.prime.todo_project.domain.task.repository.TaskRepository;
import com.prime.todo_project.infrastructure.exceptions.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TaskServiceImplTest {

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private ProjectRepository projectRepository;

    @Mock
    private TaskMapper taskMapper;

    @InjectMocks
    private TaskServiceImpl taskService;

    private TaskRequestDTO taskRequestDTO;
    private TaskResponseDTO taskResponseDTO;
    private Project project;
    private Task task;

    @BeforeEach
    public void init() {
        taskRequestDTO = new TaskRequestDTO("Task1","desc1",true, LocalDate.now(),1,1L);
        project = new Project(1L,"project1");
        task = new Task("Task1","task1",true);
        taskResponseDTO = new TaskResponseDTO(1L,"Task1","desc1",true, LocalDate.now(),1,project);
    }




    @Test
    public void createTaskReturnsTaskResponseDTO(){
        when(taskMapper.toEntity(taskRequestDTO)).thenReturn(task);
        task.setProject(project);
        when(projectRepository.findById(taskRequestDTO.project())).thenReturn(Optional.of(project));

        TaskResponseDTO taskResponseDTO = Mockito.mock(TaskResponseDTO.class);
        when(taskRepository.save(Mockito.any())).thenReturn(task);
        when(taskMapper.toResponseDTO(task)).thenReturn(taskResponseDTO);
        assertNotNull(taskService.save(taskRequestDTO));
    }

    @Test
    public void createTaskThrowsException() {
        TaskRequestDTO taskRequestDTO = Mockito.mock(TaskRequestDTO.class);
        when(projectRepository.findById(taskRequestDTO.project())).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> taskService.save(taskRequestDTO));
    }

    @Test
    public void getTaskByIdReturnsTaskResponseDTO() {
        Long taskId = 1L;
        task.setId(taskId);
        TaskResponseDTO taskResponseDTO = Mockito.mock(TaskResponseDTO.class);

        when(taskRepository.findById(taskId)).thenReturn(Optional.of(task));
        when(taskMapper.toResponseDTO(task)).thenReturn(taskResponseDTO);
        assertNotNull(taskService.getById(taskId));
    }

    @Test
    public void getTaskByIdThrowsException() {
        Long taskId = 1L;
        when(taskRepository.findById(taskId)).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> taskService.getById(taskId));
    }

    @Test
    public void getTasksReturnsPageOfTaskRequests() {
        Pageable pageable = Pageable.ofSize(1).withPage(1);
        Page<Task> tasks = Mockito.mock(Page.class);

        when(taskRepository.findAll(pageable)).thenReturn(tasks);
        when(taskMapper.toResponseDTO(Mockito.mock(Task.class))).thenReturn(taskResponseDTO);
        assertNotNull(taskService.get(pageable));
    }

    @Test
    public void updateTaskReturnsTaskResponse() {
        Long taskId = 1L;
        task.setProject(project);
        TaskResponseDTO taskResponseDTO = Mockito.mock(TaskResponseDTO.class);
        when(taskRepository.findById(taskId)).thenReturn(Optional.of(task));
        when(projectRepository.findById(taskRequestDTO.project())).thenReturn(Optional.of(project));
        when(taskMapper.toResponseDTO(task)).thenReturn(taskResponseDTO);
        assertNotNull(taskService.update(taskId,taskRequestDTO));
    }

    @Test
    public void updateTaskThrowsExceptionForTask() {
        Long taskId = 1L;
        TaskRequestDTO taskRequestDTO = Mockito.mock(TaskRequestDTO.class);

        when(taskRepository.findById(taskId)).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> taskService.update(taskId,taskRequestDTO));
    }

    @Test
    public void updateTaskThrowsExceptionForProject() {
        Long taskId = 1L;
        task.setProject(project);
        when(taskRepository.findById(taskId)).thenReturn(Optional.of(task));
        when(projectRepository.findById(taskRequestDTO.project())).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> taskService.update(taskId,taskRequestDTO));
    }

    @Test
    public void deleteTaskTest() {
        Long taskId = 1L;

        when(taskRepository.existsById(taskId)).thenReturn(true);
        taskService.delete(taskId);
        verify(taskRepository).deleteById(taskId);
    }

    @Test
    public void deleteTaskThrowsErrorTest() {
        Long taskId = 1L;

        when(taskRepository.existsById(taskId)).thenReturn(false);
        assertThrows(EntityNotFoundException.class, () -> taskService.delete(taskId));
    }

}
