package com.prime.todo_project.domain.task.service;

import com.prime.todo_project.domain.task.dto.TaskRequestDTO;
import com.prime.todo_project.domain.task.dto.TaskResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TaskService {
    TaskResponseDTO save(TaskRequestDTO taskRequestDTO);


    TaskResponseDTO getById(Long id);

    Page<TaskResponseDTO> get(Pageable pageable);

    TaskResponseDTO update(Long id, TaskRequestDTO taskRequestDTO);

    void delete(Long id);

    List<TaskResponseDTO> getForDueDate(String period);

    Page<TaskResponseDTO> getHighestPriorityTasks(Long id,Pageable pageable);

    Page<TaskResponseDTO> searchByTitle(String name,Pageable pageable);

    Page<TaskResponseDTO> getUnfinishedCloseToDeadline(Pageable pageable);

    TaskResponseDTO markAsDone(Long id);
}
