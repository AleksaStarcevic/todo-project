package com.prime.todo_project.domain.task.mapper;

import com.prime.todo_project.domain.task.dto.TaskRequestDTO;
import com.prime.todo_project.domain.task.dto.TaskResponseDTO;
import com.prime.todo_project.domain.task.entity.Task;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface TaskMapper {
    @Mapping(target = "project", ignore = true)
    Task toEntity(TaskRequestDTO taskRequestDTO);

    TaskResponseDTO toResponseDTO(Task Task);

    @Mapping(target = "project", ignore = true)
    void update(@MappingTarget Task task, TaskRequestDTO taskRequestDTO);
}
