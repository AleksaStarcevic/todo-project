package com.prime.todo_project.domain.task.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.prime.todo_project.domain.project.entity.Project;

import java.time.LocalDate;

public record TaskResponseDTO(
        Long id,
        String name,
        String description,
        Boolean taskDone,
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
        LocalDate dueDate,
        Integer priority,
        Project project
) {
}
