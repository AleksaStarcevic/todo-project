package com.prime.todo_project.domain.task.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.prime.todo_project.domain.project.entity.Project;
import jakarta.persistence.*;

import java.time.LocalDate;

@Entity
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name",nullable = false)
    private String name;
    @Column(name = "description")
    private String description;

    @Column(name = "taskDone",columnDefinition = "boolean default false")
    private Boolean taskDone;

    @Column(name = "dueDate",nullable = false)
    @Temporal(TemporalType.DATE)
    private LocalDate dueDate;

    @Column(name = "priority",nullable = false,columnDefinition = "int default 4")
    private Integer priority;

    @ManyToOne
    @JoinColumn(name = "project_id")
    @JsonIgnoreProperties("tasks")
    private Project project;

    public Boolean getTaskDone() {
        return taskDone;
    }

    public void setTaskDone(Boolean taskDone) {
        this.taskDone = taskDone;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", taskDone=" + taskDone +
                ", project=" + project +
                '}';
    }

    public Task() {
    }

    public Task( String name, String description, Boolean taskDone) {
        this.name = name;
        this.description = description;
        this.taskDone = taskDone;
    }

    public Task(Long id, String name, String description, Boolean taskDone, Project project) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.taskDone = taskDone;
        this.project = project;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
