package com.prime.todo_project.domain.task.dto;

import jakarta.validation.constraints.FutureOrPresent;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;

import java.time.LocalDate;

public record TaskRequestDTO(
        @NotBlank(message = "Name is mandatory")
        @Size(min = 3, message = "Name must have at least 3 characters")
        String name,
        String description,
        Boolean taskDone,
        @FutureOrPresent(message = "Date must be in future")
        LocalDate dueDate,
        @Positive(message = "Priority must be a positive number")
        Integer priority,
        Long project
) {

}
