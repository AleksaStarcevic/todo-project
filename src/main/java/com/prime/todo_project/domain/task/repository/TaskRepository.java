package com.prime.todo_project.domain.task.repository;

import com.prime.todo_project.domain.project.entity.Project;
import com.prime.todo_project.domain.task.entity.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task,Long> {
    List<Task> findAllByDueDate(LocalDate date);
    List<Task> findAllByDueDateBetween(LocalDate startDate, LocalDate endDate);
    Page<Task> findAllByProjectAndPriorityEquals(Project project, Integer priority, Pageable pageable);
    Page<Task> findAllByNameContainingIgnoreCase(String name,Pageable pageable);
    @Query(value = "select t from Task t where t.taskDone = false and t.dueDate <= :date and year(:date) = year(t.dueDate)")
    Page<Task> findAllUnfinishedCloseToDeadline(LocalDate date,Pageable pageable);

    @Transactional
    @Modifying
    @Query(value = "update Task t set t.taskDone = true")
    void setTaskToDone();
}
