package com.prime.todo_project.domain.task.service;

import com.prime.todo_project.domain.project.entity.Project;
import com.prime.todo_project.domain.project.repository.ProjectRepository;
import com.prime.todo_project.domain.task.dto.TaskRequestDTO;
import com.prime.todo_project.domain.task.dto.TaskResponseDTO;
import com.prime.todo_project.domain.task.entity.Task;
import com.prime.todo_project.domain.task.mapper.TaskMapper;
import com.prime.todo_project.domain.task.repository.TaskRepository;
import com.prime.todo_project.infrastructure.exceptions.EntityNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.prime.todo_project.constants.Constants.*;

@Service
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;
    private final ProjectRepository projectRepository;
    private final TaskMapper taskMapper;

    public TaskServiceImpl(TaskRepository taskRepository, ProjectRepository projectRepository, TaskMapper taskMapper) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.taskMapper = taskMapper;
    }

    @Override
    public TaskResponseDTO save(TaskRequestDTO taskRequestDTO) {
        Project project = projectRepository.findById(taskRequestDTO.project())
                .orElseThrow(()-> new EntityNotFoundException(Project.class.getSimpleName(),"id",taskRequestDTO.project()));
        Task task = taskMapper.toEntity(taskRequestDTO);
        task.setProject(project);
        taskRepository.save(task);
        return taskMapper.toResponseDTO(task);
    }

    @Override
    public TaskResponseDTO getById(Long id) {
        Task task = taskRepository.findById(id)
                .orElseThrow(()-> new EntityNotFoundException(Task.class.getSimpleName(),"id",id));

        return taskMapper.toResponseDTO(task);
    }

    @Override
    public Page<TaskResponseDTO> get(Pageable pageable) {
        return taskRepository.findAll(pageable)
                .map(taskMapper::toResponseDTO);
    }

    @Override
    public TaskResponseDTO update(Long id, TaskRequestDTO taskRequestDTO) {
        Task task = taskRepository.findById(id)
                .orElseThrow(()-> new EntityNotFoundException(Task.class.getSimpleName(),"id",id));
        Project project = projectRepository.findById(taskRequestDTO.project())
                .orElseThrow(()-> new EntityNotFoundException(Project.class.getSimpleName(),"id",taskRequestDTO.project()));
        task.setProject(project);
        taskMapper.update(task,taskRequestDTO);
        taskRepository.save(task);
        return taskMapper.toResponseDTO(task);
    }

    @Override
    public void delete(Long id) {
        if(!taskRepository.existsById(id)) throw new EntityNotFoundException(Task.class.getSimpleName(),"id",id);
        taskRepository.deleteById(id);
    }

    @Override
    public List<TaskResponseDTO> getForDueDate(String period) {
        List<Task> tasks;
        LocalDate todayDate = LocalDate.now();

        switch (period) {
            case DUE_DATE_TODAY:
                tasks = taskRepository.findAllByDueDate(todayDate);
                break;
            case DUE_DATE_NEXT_SEVEN_DAYS:
                tasks = taskRepository.findAllByDueDateBetween(todayDate, todayDate.plusDays(7));
                break;
            default:
                tasks = Collections.emptyList();
                break;
        }
        return tasks.stream().map(taskMapper::toResponseDTO).collect(Collectors.toList());
    }

    @Override
    public Page<TaskResponseDTO> getHighestPriorityTasks(Long id,Pageable pageable) {
        if(!projectRepository.existsById(id)) throw new EntityNotFoundException(Project.class.getSimpleName(),"id",id);
        Project project = projectRepository.findById(id).get();
        Page<Task> highestPriorityTasks =  taskRepository.findAllByProjectAndPriorityEquals(project,TASK_HIGHEST_PRIORITY,pageable);
        return new PageImpl<>(highestPriorityTasks.stream().map(taskMapper::toResponseDTO).collect(Collectors.toList()));
    }

    @Override
    public Page<TaskResponseDTO> searchByTitle(String name,Pageable pageable) {
        Page<Task> searchedTasks = taskRepository.findAllByNameContainingIgnoreCase(name,pageable);
        return new PageImpl<>(searchedTasks.stream().map(taskMapper::toResponseDTO).collect(Collectors.toList()));
    }

    @Override
    public Page<TaskResponseDTO> getUnfinishedCloseToDeadline(Pageable pageable) {
        Page<Task> unfinishedTasks = taskRepository.findAllUnfinishedCloseToDeadline(LocalDate.now().plusDays(2),pageable);
        return new PageImpl<>(unfinishedTasks.stream().map(taskMapper::toResponseDTO).collect(Collectors.toList()));
    }

    @Override
    public TaskResponseDTO markAsDone(Long id) {
        if(!taskRepository.existsById(id)) throw new EntityNotFoundException(Task.class.getSimpleName(),"id",id);
        taskRepository.setTaskToDone();
        return taskMapper.toResponseDTO(taskRepository.findById(id).get());
    }


}
