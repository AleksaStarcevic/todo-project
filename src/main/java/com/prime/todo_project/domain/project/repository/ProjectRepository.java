package com.prime.todo_project.domain.project.repository;

import com.prime.todo_project.domain.project.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project,Long> {
}
