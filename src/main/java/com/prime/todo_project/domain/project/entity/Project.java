package com.prime.todo_project.domain.project.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.prime.todo_project.domain.task.entity.Task;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name",nullable = false)
    private String name;

    @OneToMany(mappedBy = "project",cascade = CascadeType.ALL)
    @JsonIgnoreProperties("project")
    private List<Task> tasks= new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }


    public Project() {
    }

    public Project(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
