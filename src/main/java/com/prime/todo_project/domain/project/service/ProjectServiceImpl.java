package com.prime.todo_project.domain.project.service;

import com.prime.todo_project.domain.project.dto.ProjectRequestDTO;
import com.prime.todo_project.domain.project.dto.ProjectResponseDTO;
import com.prime.todo_project.domain.project.entity.Project;
import com.prime.todo_project.domain.project.mapper.ProjectMapper;
import com.prime.todo_project.domain.project.repository.ProjectRepository;
import com.prime.todo_project.infrastructure.exceptions.EntityNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
public class ProjectServiceImpl implements ProjectService{

    private final ProjectMapper projectMapper;
    private final ProjectRepository projectRepository;

    public ProjectServiceImpl(ProjectMapper projectMapper, ProjectRepository projectRepository) {
        this.projectMapper = projectMapper;
        this.projectRepository = projectRepository;
    }

    @Override
    public ProjectResponseDTO save(ProjectRequestDTO projectRequestDTO) {
        Project project = projectMapper.projectRequestToProject(projectRequestDTO);
        project.getTasks().forEach(task -> task.setProject(project));
        projectRepository.save(project);
        return projectMapper.projectToProjectResponse(project);
    }

    @Override
    public ProjectResponseDTO getById(Long id) {
        Project project = projectRepository.findById(id)
                .orElseThrow(()-> new EntityNotFoundException(Project.class.getSimpleName(),"id",id));

        return projectMapper.projectToProjectResponse(project);
    }

    @Override
    public Page<ProjectResponseDTO> get(Pageable pageable) {
        return projectRepository.findAll(pageable)
                .map(projectMapper::projectToProjectResponse);
    }

    @Override
    public ProjectResponseDTO update(Long id, ProjectRequestDTO projectRequestDTO) {
        Project project = projectRepository.findById(id)
                .orElseThrow(()-> new EntityNotFoundException(Project.class.getSimpleName(),"id",id));
        project.setName(projectRequestDTO.name());
        project.setTasks(projectRequestDTO.tasks());
        project.getTasks().forEach(task -> task.setProject(project));
        projectRepository.save(project);
        return projectMapper.projectToProjectResponse(project);
    }

    @Override
    public void delete(Long id) {
        if(!projectRepository.existsById(id)) throw new EntityNotFoundException(Project.class.getSimpleName(),"id",id);
        projectRepository.deleteById(id);
    }

}
