package com.prime.todo_project.domain.project.service;

import com.prime.todo_project.domain.project.dto.ProjectRequestDTO;
import com.prime.todo_project.domain.project.dto.ProjectResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface ProjectService {
    ProjectResponseDTO save(ProjectRequestDTO project);

    ProjectResponseDTO getById(Long id);

    Page<ProjectResponseDTO> get(Pageable pageable);

    ProjectResponseDTO update(Long id, ProjectRequestDTO projectRequestDTO);

    void delete(Long id);
}
