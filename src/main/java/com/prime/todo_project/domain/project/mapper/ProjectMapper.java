package com.prime.todo_project.domain.project.mapper;

import com.prime.todo_project.domain.project.dto.ProjectRequestDTO;
import com.prime.todo_project.domain.project.dto.ProjectResponseDTO;
import com.prime.todo_project.domain.project.entity.Project;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProjectMapper {

    Project projectRequestToProject(ProjectRequestDTO projectRequestDTO);

    ProjectResponseDTO projectToProjectResponse(Project project);




}
