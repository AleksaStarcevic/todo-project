package com.prime.todo_project.domain.project.dto;

import com.prime.todo_project.domain.task.entity.Task;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

import java.util.List;

public record ProjectRequestDTO(
        @NotBlank(message = "Name is mandatory")
        @Size(min = 3, message = "Name must have at least 3 characters")
        String name,

        @NotEmpty(message = "List of tasks must contain at least one element")
        List< @Valid Task> tasks
) {}

