package com.prime.todo_project.domain.project.dto;

import com.prime.todo_project.domain.task.entity.Task;

import java.util.List;

public record ProjectResponseDTO(
        Long id,
        String name,
        List<Task> tasks
) {
}
