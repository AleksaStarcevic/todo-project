package com.prime.todo_project.web;

import com.prime.todo_project.domain.task.dto.TaskRequestDTO;
import com.prime.todo_project.domain.task.dto.TaskResponseDTO;
import com.prime.todo_project.domain.task.service.TaskService;
import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.prime.todo_project.constants.Constants.DUE_DATE_LIST;

@RestController
@RequestMapping("/api/v1/tasks")
public class TaskController {

    private final TaskService taskService;


    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping
    public ResponseEntity<TaskResponseDTO> save(@Valid @RequestBody TaskRequestDTO taskRequestDTO){
        return  ResponseEntity.status(HttpStatus.CREATED).body(taskService.save(taskRequestDTO));
    }

    @GetMapping("/{id}")
    public ResponseEntity<TaskResponseDTO> getById(@PathVariable Long id) {
        return ResponseEntity.ok(taskService.getById(id));
    }

    @GetMapping
    public ResponseEntity<Page<TaskResponseDTO>> get(Pageable pageable) {
        return ResponseEntity.ok(taskService.get(pageable));
    }

    @PutMapping("/{id}")
    public ResponseEntity<TaskResponseDTO> update(@PathVariable Long id, @RequestBody TaskRequestDTO taskRequestDTO) {
        return ResponseEntity.ok(taskService.update(id, taskRequestDTO));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        taskService.delete(id);
    }

    @GetMapping("/dueDate")
    public ResponseEntity<List<TaskResponseDTO>> getForDueDate(@RequestParam String period) {
        if(!DUE_DATE_LIST.contains(period)) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(taskService.getForDueDate(period));
    }

    @GetMapping("/search")
    public ResponseEntity<Page<TaskResponseDTO>> searchByTitle(@RequestParam String name,Pageable pageable) {
        return ResponseEntity.ok(taskService.searchByTitle(name,pageable));
    }

    @GetMapping("/important_tasks")
    public ResponseEntity<Page<TaskResponseDTO>> getUnfinishedCloseToDeadline(Pageable pageable) {
        return ResponseEntity.ok(taskService.getUnfinishedCloseToDeadline(pageable));
    }

    @PatchMapping("/{id}")
    public ResponseEntity<TaskResponseDTO> markAsDone(@PathVariable Long id) {
        return ResponseEntity.ok(taskService.markAsDone(id));
    }
}
