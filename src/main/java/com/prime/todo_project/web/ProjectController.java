package com.prime.todo_project.web;

import com.prime.todo_project.domain.project.dto.ProjectRequestDTO;
import com.prime.todo_project.domain.project.dto.ProjectResponseDTO;
import com.prime.todo_project.domain.project.service.ProjectService;
import com.prime.todo_project.domain.task.dto.TaskResponseDTO;
import com.prime.todo_project.domain.task.service.TaskService;
import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/api/v1/projects")
public class ProjectController {

    private final ProjectService projectService;
    private final TaskService taskService;

    public ProjectController(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @PostMapping
    public ResponseEntity<ProjectResponseDTO> save(@Valid @RequestBody ProjectRequestDTO project){
       return  ResponseEntity.status(HttpStatus.CREATED).body(projectService.save(project));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProjectResponseDTO> getById(@PathVariable Long id) {
        return ResponseEntity.ok(projectService.getById(id));
    }

    @GetMapping
    public ResponseEntity<Page<ProjectResponseDTO>> get(Pageable pageable) {
        return ResponseEntity.ok(projectService.get(pageable));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProjectResponseDTO> update(@PathVariable Long id, @RequestBody ProjectRequestDTO projectRequestDTO) {
        return ResponseEntity.ok(projectService.update(id, projectRequestDTO));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
       projectService.delete(id);
    }

    @GetMapping("/{id}/tasks/highest_priority")
    public ResponseEntity<Page<TaskResponseDTO>> getHighestPriorityTasks(@PathVariable Long id,Pageable pageable) {
        return ResponseEntity.ok(taskService.getHighestPriorityTasks(id,pageable));
    }
}
