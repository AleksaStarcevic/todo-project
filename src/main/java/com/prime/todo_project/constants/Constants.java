package com.prime.todo_project.constants;


import java.util.List;

public class Constants {

    public static final String DUE_DATE_TODAY = "today";
    public static final String DUE_DATE_NEXT_SEVEN_DAYS = "seven_days";
    public static final List DUE_DATE_LIST = List.of(DUE_DATE_TODAY,DUE_DATE_NEXT_SEVEN_DAYS);

    public static final Integer TASK_HIGHEST_PRIORITY = 1;
    public static final Integer TASK_LOWEST_PRIORITY =  4;
}
