package com.prime.todo_project.infrastructure.exceptions;

public class EntityNotFoundException extends RuntimeException{

    public EntityNotFoundException(String entity, String field, Long value) {
        super(entity + " with " + field + " " + value + " doesn't exist");
    }
}
